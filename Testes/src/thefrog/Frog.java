package thefrog;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class Frog {

	private Scanner sc = new Scanner(System.in);
	private int distance;
	Set<String> MovementList = new LinkedHashSet<>();
	
	public static void main(String[] args) {
		Frog f = new Frog();
		f.readDistance();
		f.calculateMoves();
		f.printMoves();

		
	}

	
	public int readDistance(){
		int distance = 0;
		try{
			System.out.print("Inform the distance: ");
			distance = Integer.parseInt(sc.next());
		}catch(NumberFormatException e){
			System.out.println("Only numbers allowed. Please enter a valid number.");
			distance = readDistance();
		}
		this.distance = distance;
		return distance;
	}
	
	
	
	public String calculateMoves(){
		String steps = "";
		String[] lista = new String[distance];
		Set<String> teste = new HashSet<>();
		for(int i = 0; i<distance; i++){
			lista[i] = "1";	
		}
		
		String item = "";
		for(int i = 0 ; i<lista.length; i++){
			item = "";
			for(String str : lista){
				if(str.equals("1")){
					item += "1";
				}
				
				if(str.equals("2")){
					item += "2";
				}
			}
			teste.add(item);
		
			if(i < lista.length-1 && i%2 == 0){
				lista[(i+1)] = "2"; 
				lista[i] = "0";
			}
		}
		
		for(String str : teste){
			makeAnagram(str.toCharArray(),  0);
		}
		return steps;
	}
	
	
	
	public String makeAnagram(char[] a, int i) {
		String moves = ""; 
		if (i == a.length-1){
			moves = changeToWord(a);
			MovementList.add(moves); 
		}else {
			for (int j=i; j< a.length; j++) {
				//swap a[i] with a[j]
				char c = a[i]; 
				a[i] = a[j]; 
				a[j] = c;
				makeAnagram(a, i+1);
				//swap back
				c = a[i]; 
				a[i] = a[j]; 
				a[j] = c;
			}
		}
		return moves;
	}
	
		
	public String changeToWord(char[] a){
		String word = "";
		int i = 0;
		for(char c : a){
			word += c;
			if(i < a.length-1){
				word += "-";
			}
			i++;
		}
		word = word.replaceAll("1", "step");
		word = word.replaceAll("2", "jump");
		return word;
	}

	
	public void printMoves(){
		for(String str : this.MovementList){
			System.out.println(str);
		}
	}
	
}
