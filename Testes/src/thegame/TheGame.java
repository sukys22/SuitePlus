package thegame;
import java.util.Random;
import java.util.Scanner;


public class TheGame {

	Scanner sc = new Scanner(System.in);
	String nome;
	int escolhaPlayer;
	int escolhaPc;
	
	public static void main(String[] args) {
		TheGame st = new TheGame();
		st.requestPlayerName();

		do{
			st.resetGame();
			st.playerMakesAChoice();
			st.computerMakesAChoice();
			st.printScore();
		}while(st.escolhaPlayer < 4);
		
	}
	

	
	public void requestPlayerName(){
		System.out.print("Digite seu nome: ");
		String nome = sc.next();
		if(nome == null){
			nome = "Humano";
		}
		this.nome = nome;
	}
	
	public void resetGame(){
		this.escolhaPc = 0;
		this.escolhaPlayer = 0;
	}
	
	public void playerMakesAChoice(){
		System.out.println("1 - Rock");
		System.out.println("2 - Paper");
		System.out.println("3 - Scissors");
		System.out.println("4 - Exit");
		System.out.print("Make your choice (1, 2, 3 or 4): ");
		
		try{
			int escolha = Integer.parseInt(sc.next());
			this.escolhaPlayer = escolha;
		}catch(NumberFormatException e){
			System.out.println("Please type a valid number.");
			playerMakesAChoice();
		}
		System.out.println("\n");
	}
	
	
	public int computerMakesAChoice(){
		int escolha = 0;
		Random random = new Random();

		do{
			escolha = random.nextInt(4);
		}while(escolha < 1 || escolha > 3);
		
		this.escolhaPc = escolha;
		return escolha;
	}
	
	
	public String compareSelections(){
		String msg;
		
		if(escolhaPc == escolhaPlayer){
			msg = "It's a draw.";
		}else if(escolhaPlayer > escolhaPc){
			if(escolhaPlayer == 3 && escolhaPc == 1){
				msg = "Computer Wins.";
			}else{
				msg = this.nome + " Wins.";
			}
		}else{
			if(escolhaPc == 3 && escolhaPlayer == 1){
				msg = this.nome + " Wins.";
			}else{
				msg = "Computer Wins.";
			}
		}
		return msg;
	}
	
	
	public String diplaySelecao(int escolha){
		String msg;
		switch (escolha) {
		case 1:
			msg = "Rock";
			break;
			
		case 2:
			msg = "Paper";
			break;
			
		case 3:
			msg = "Scissors";
			break;

		case 4:
			msg = "Exiting system.";
			break;
			
		default:
			msg = "Option not available. Choose again.";
			break;
		}
		return msg;
	}


	public void printScore(){
		System.out.println("  -  ");
		System.out.println(nome + " choose " + diplaySelecao(escolhaPlayer) + ".");
		System.out.println("Computer choose " +  diplaySelecao(escolhaPc) + ".");
		System.out.println(compareSelections());
		System.out.println("  -  ");
	}
}

